import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const firebaseConfig = {
    apiKey: "AIzaSyCzC9pDK16Ma18ToIXPwlJQn8VJooDa0Ug",
    authDomain: "auth-416f5.firebaseapp.com",
    projectId: "auth-416f5",
    storageBucket: "auth-416f5.appspot.com",
    messagingSenderId: "254849974311",
    appId: "1:254849974311:web:651a9b07a1df0d334ace02",
    measurementId: "G-145TCVJDES"
}
const appFirebase = firebase.initializeApp(firebaseConfig)
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
const dbFirebase = appFirebase.firestore();

const saveDataCollection = async (nameCollection, dataCollection) => {
    return await dbFirebase.collection(nameCollection).doc().set(dataCollection)
}

const getDataCollection = async (nameCollection) => {
    return await dbFirebase.collection(nameCollection).get()
}

const getDataOnSnapShot = async (nameCollection, eventEmmiter) => {
    return await dbFirebase.collection(nameCollection).onSnapshot(eventEmmiter)
}

const deleteDataCollection = async (nameCollection, id) => {
    return await dbFirebase.collection(nameCollection).doc(id).delete()
}

const getByIdColletion = async (nameCollection, id) => {
    return await dbFirebase.collection(nameCollection).doc(id).get()
}

const updateDataCollection = async (nameCollection, id, data) => {
    return await dbFirebase.collection(nameCollection).doc(id).update(data)
}

export {
    appFirebase,
    googleAuthProvider,
    dbFirebase,
    saveDataCollection,
    getDataCollection,
    getDataOnSnapShot,
    deleteDataCollection,
    getByIdColletion,
    updateDataCollection
}