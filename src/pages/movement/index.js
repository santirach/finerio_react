import React, { useEffect, useState } from "react"
import useMovementPageState from "./state";
import ListComponent from "../../components/list";
import { getUserLocal } from "../../utils/auth/user.util";
import { Spin } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';

const MovementPage = () => {
    const {
        movements,
        handleGetMoreInformationRequest
    } = useMovementPageState()

    const { extraInformation } = JSON.parse(getUserLocal()) || {}

    const [isFetching, setIsFetching] = useState(false);

    const handlerMoreInformationAction = () => {
        setIsFetching(true)
    }

    const antIcon = <LoadingOutlined style={{ fontSize: 24 }} spin />

    useEffect(() => {
        if (isFetching) {
            handleGetMoreInformationRequest({
                offset: movements.length,
                max: movements.length + 10
            })
            setTimeout(() => {
                setIsFetching(false)
            }, 400)

        }
    }, [isFetching, movements])

    return (
        <div className="movement-form">
            <div className="movement-form__content">
                <div className="movement-form__content__information">
                    <span className="movement-form__content__information__title">Movimientos realizados</span>
                    <label className="movement-form__content__information__email">{extraInformation.email}</label>
                </div>
            </div>
            <div className="movement-form__list">
                <ListComponent
                    movements={movements}
                    getMoreInformation={handlerMoreInformationAction} />
                {isFetching && <Spin indicator={antIcon} />}
            </div>
        </div>

    )
}

export default MovementPage