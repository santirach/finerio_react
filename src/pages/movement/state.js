import { useEffect, useState } from "react"
import { useHistory } from "react-router"
import { GET_MOVEMENTS } from "../../services/movement.services"
import { removeToken } from "../../utils/auth/auth.util"
import { getUserLocal, removeUserLocal } from "../../utils/auth/user.util"


const useMovementPageState = () => {
    const [movements, setMovements] = useState([])
    const history = useHistory()
    useEffect(() => {
        if (movements.length == 0) {
            handleGetMoreInformationRequest({
                offset: 0,
                max: 10
            })
        }
    }, [movements])

    const handleGetMoreInformationRequest = ({ offset, max }) => {
        const { extraInformation } = JSON.parse(getUserLocal())
        GET_MOVEMENTS({ idUser: extraInformation.id, offset, max }).then((response) => {
            setMovements([...movements, ...response.data.data])
        }).catch(() => {
            removeToken()
            removeUserLocal()
            history.push('/login');
        })
    }

    return {
        movements,
        setMovements,
        handleGetMoreInformationRequest
    }
}

export default useMovementPageState