import React, { useContext, useEffect, useState } from "react"
import { Input, Button } from 'antd';
import { EyeInvisibleOutlined, EyeTwoTone } from '@ant-design/icons';
import { UserOutlined } from '@ant-design/icons';
import useLoginPageState from "./state";
import whitAuthHOC from "../../HOC/auth.hoc";
import AuthContext from "../../contexts/auth.context";
import { isEmpty } from "lodash";
import { useHistory } from "react-router";
import { Alert } from 'antd';

const LoginPage = () => {
    const history = useHistory()
    const context = useContext(AuthContext)
    const {
        userAuthLogged,
        setUserAuthLogged
    } = context
    const [userInformation, setUserInformation] = useState({
        email: '',
        password: ''
    })
    const {
        handlerLoginActionRequest,
        resultAuth,
    } = useLoginPageState({ setUserAuthLogged })

    const [isSend, setIsSend] = useState(false)

    const handlerFormAction = (event, type) => {
        setUserInformation({ ...userInformation, [type]: event.target.value })
    }

    useEffect(() => {
        if (!isEmpty(userAuthLogged)) {
            history.push('/movementPage');
        }
    }, [userAuthLogged])

    const handlerPostLonginAction = () => {
        setIsSend(true)
        if (userInformation.email != '' && userInformation.password != '') {
            handlerLoginActionRequest(userInformation)
        }

    }


    return (
        <div className="login-form">
            <div className="login-form__title">
                <span>Bienvenido a tus movimientos</span>
            </div>
            <div className="login-form__inputs">
                <div>
                    <Input
                        onChange={(e) => handlerFormAction(e, 'email')}
                        placeholder="Email"
                        prefix={<UserOutlined />} />
                    {isSend && userInformation.email == '' && <span className="message__warning" >Informacion es requerida</span>}
                </div>
                <div>
                    <Input.Password
                        onChange={(e) => handlerFormAction(e, 'password')}
                        placeholder="Password"
                        iconRender={visible => (visible ? <EyeTwoTone /> : <EyeInvisibleOutlined />)}
                    />
                    {isSend && userInformation.password == '' && <span className="message__warning" >Informacion es requerida</span>}
                </div>
                <Button type="primary" onClick={() => handlerPostLonginAction()}>Login</Button>
            </div>
            {resultAuth.error && <Alert
                className="alerts__position"
                message="Usuario no existe"
                type="warning"
                showIcon
                closable
            />}
        </div>
    )
}

export default whitAuthHOC(LoginPage)