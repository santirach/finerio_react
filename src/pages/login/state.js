import { useEffect, useState } from "react"
import { GET_INFORMATION_EXTRA_USER, POST_AUTH } from "../../services/auth.service"
import { setToken } from "../../utils/auth/auth.util"

const useLoginPageState = ({ setUserAuthLogged }) => {
    const [resultAuth, setResultAuht] = useState({
        succes: false,
        error: false,
    })


    const handlerLoginActionRequest = (userInformation) => {
        POST_AUTH({ password: userInformation.password, username: userInformation.email }).then((response) => {
            setResultAuht({ succes: true, error: false })
            setToken(response.data.access_token)
            GET_INFORMATION_EXTRA_USER().then((responseExtra) => {
                setUserAuthLogged({ user: response.data, extraInformation: responseExtra.data })
            })

        }).catch(() => {
            setResultAuht({ succes: false, error: true })
        })
    }

    useEffect(() => {
        if (resultAuth.error) {
            setTimeout(() => {
                setResultAuht({ succes: false, error: false })
            }, 2000)
        }
    }, [resultAuth])

    return {
        handlerLoginActionRequest,
        resultAuth
    }
}

export default useLoginPageState