export const config = {
  host: process.env.HOST,
  endpoints: {
    auth: {
      post: '/login',
    },
    user: {
      getExtraInformationUser: '/me',
    },
    movement: {
      getAll: '/users/${idUser}/movements?deep=true&offset=${offset}&max=${max}&includeCharges=true&includeDeposits=true&includeDuplicates=false'
    }
  },
}
