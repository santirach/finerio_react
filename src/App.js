import React from 'react'
import './App.css';
import Router from './routes/components/router.route';
import './stylesheets/ant.less'
function App() {
  return (
    <div className="App">
      <Router />
    </div>
  );
}

export default App;
