import React, { useEffect, useState } from 'react'
import { isEmpty } from 'lodash'
import AuthContext from '../contexts/auth.context'
import { setToken } from '../utils/auth/auth.util'
import { getUserLocal, setUserLocal } from '../utils/auth/user.util'

const whitAuthHOC = (WrappedComponent) => {
    const authHOC = ({
        ...props
    }) => {

        const [userAuthLogged, setUserAuthLogged] = useState({})

        useEffect(() => {
            if (!isEmpty(userAuthLogged) && getUserLocal() == null) {
                setUserLocal(JSON.stringify(userAuthLogged))
            } else if (isEmpty(userAuthLogged) && getUserLocal() != null) {
                setUserAuthLogged(JSON.parse(getUserLocal()))
            }
        }, [userAuthLogged])

        return (
            <AuthContext.Provider
                value={{
                    name: 'santiago',
                    userAuthLogged,
                    setUserAuthLogged
                }}
            >
                <WrappedComponent {...props} />
            </AuthContext.Provider>
        )

    }

    return authHOC
}

export default whitAuthHOC
