import React from 'react'
import { Route, Redirect } from 'react-router-dom'
import PropTypes from 'prop-types'

function RouteWithSubRoutes(route) {

  return (
    <Route
      path={route.path}
      render={props =>
        // pass the sub-routes down to keep nesting
        route.isAuthenticated ? (
          <route.component {...props} routes={route.routes} />
        ) : (
          <Redirect to={{ pathname: '/login' }} />
        )
      }
    />
  )
}

RouteWithSubRoutes.propTypes = {
  routes: PropTypes.arrayOf(PropTypes.object),
}

export default RouteWithSubRoutes
