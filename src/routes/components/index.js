import React from 'react'
import { Layout } from 'antd'
import { Switch, Route, useHistory, Redirect } from 'react-router-dom'
import RouteWithSubRoutes from './routeWithSubRoutes.route'
import routes from '../routes'
import LoginPage from '../../pages/login'
import { getUserLocal } from '../../utils/auth/user.util'

const { Content } = Layout

function LayoutApp() {
  const history = useHistory()
  const isAuthenticated = getUserLocal() ? true : false
  const urlPermision = (isAuthenticated) => {
    return (
      <>
        {isAuthenticated && (
          <Switch>
            {routes.map(route => (
              <RouteWithSubRoutes
                key={route.path}
                {...route}
                isAuthenticated={isAuthenticated}
              />
            ))}
            <Redirect from="*" exact={true} to="/movementPage" />
          </Switch>
        )}
      </>
    );
  };

  return (
    <Layout className="container">
      <Content className="container__content">
        {!isAuthenticated && (
          <Switch>
            <Route exact={true} path="/login">
              <LoginPage />
            </Route>
            <Redirect from="/" exact={true} to="/login" />
            <Redirect from="*" exact={true} to="/login" />
          </Switch>
        )}

        {urlPermision(isAuthenticated)}
      </Content>
    </Layout>
  )
}


export default LayoutApp
