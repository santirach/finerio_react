import React from 'react'
import { BrowserRouter } from 'react-router-dom'
import LayoutApp from '.'
import whitAuthHOC from '../../HOC/auth.hoc'

function Router() {

  return (
    <BrowserRouter>
      <LayoutApp />
    </BrowserRouter>
  )
}

export default whitAuthHOC(Router)
