import { Empty } from 'antd'
import MovementPage from '../pages/movement'

const routes = [
    {
        path: '/',
        component: Empty,
        exact: true,
    },
    {
        path: '/movementPage',
        component: MovementPage,
        exact: true,
    }
]
export default routes