import React, { useEffect, useRef } from "react"



const ListComponent = ({ movements = [], getMoreInformation }) => {
    const scrollContainer = useRef(null);
    useEffect(() => {
        scrollContainer.current.addEventListener('scroll', handleScroll);
        return () => scrollContainer.current.removeEventListener('scroll', handleScroll);
    }, []);

    const handleScroll = () => {
        if (scrollContainer.current.scrollHeight - scrollContainer.current.scrollTop == 250) {
            setTimeout(() => {
                getMoreInformation()
            }, 500)
        }
    }
    return (
        <div className="listComponent" ref={scrollContainer}>
            <ul>
                {movements.length > 0 && movements.map((movement, index) => {
                    return (
                        <li key={index}>
                            {movement.description}
                        </li>
                    )
                })}
            </ul>
        </div>

    )
}

export default ListComponent