import { config } from "../config/api.config"
import { OPTIONS_HTTP } from "../constants/apiConfig.constant"
import request from "../core/api.core"

export const POST_AUTH = async (item) => {
    return request(config, OPTIONS_HTTP.POST, 'auth.post', item)
}

export const GET_INFORMATION_EXTRA_USER = async () => {
    return request(config, OPTIONS_HTTP.GET, 'user.getExtraInformationUser')
}