import request from '../core/api.core'
import { config } from "../config/api.config"

export const GET_MOVEMENTS = async (item) => {
  return request(config, 'GET', 'movement.getAll', item)
}
