export const setUserLocal = userLocal => localStorage.setItem('UserLocal', userLocal)

export const getUserLocal = () => localStorage.getItem('UserLocal')

export const removeUserLocal = () => localStorage.removeItem('UserLocal')
