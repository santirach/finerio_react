import axios from 'axios'
import qs from 'qs'
import { get, isEmpty, merge, template } from 'lodash'
import { OPTIONS_HTTP } from '../constants/apiConfig.constant'
import { getToken } from '../utils/auth/auth.util'



const defaultHeaders = {
  "Content-Type": "application/json"
}

const defaultOptions = { withCredentials: true }

const matchUrlParams = (url, params) => {
  return template(url)(params)
}

const createBaseURL = (config, endpointReference, params) => {
  const url = `${config.host}${get(config, `endpoints.${endpointReference}`)}`
  return matchUrlParams(url, params)
}

const createHeaders = settings => {
  if (isEmpty(settings.headers)) {
    return { ...defaultHeaders, Authorization: `Bearer ${getToken()}` }
  }

  return merge({}, defaultHeaders, settings.headers)
}

const createConfig = (config, method, endpointReference, params, settings) => {
  const { isQueryString = false, cancelToken = null } = settings
  const defaultConfig = {
    url: createBaseURL(config, endpointReference, params),
    method,
    cancelToken,
    timeout: 1000 * 120, // Wait for 120 seconds
    headers: createHeaders(settings),
  }

  switch (method) {
    case OPTIONS_HTTP.GET:
      return {
        ...defaultConfig,
        params,
        paramsSerializer: params =>
          isQueryString ? qs.stringify(params, { encode: false }) : null,
      }
    case OPTIONS_HTTP.POST:
      return {
        ...defaultConfig,
        data: params,
      }
    case OPTIONS_HTTP.DELETE:
      return {
        ...defaultConfig,
        data: params,
        params,
      }
    case OPTIONS_HTTP.PUT:
      return {
        ...defaultConfig,
        data: params,
        params,
      }
    case OPTIONS_HTTP.PATCH:
      return {
        ...defaultConfig,
        data: params,
        params,
      }

    default:
      return {
        ...defaultConfig
      }
  }

}

const request = async (
  config,
  method,
  endpointReference,
  params,
  settings = {},
) => {
  const res = await axios(
    createConfig(config, method, endpointReference, params, settings),
    isEmpty(settings.options) ? defaultOptions : { ...defaultOptions, ...settings.options }
  )
  return res;
}

export default request
export const cancelToken = axios.CancelToken
