const path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const Dotenv = require('dotenv-webpack');
const dotenv = require('dotenv');
const Webpack = require('webpack');
const FaviconsWebpackPlugin = require('favicons-webpack-plugin');
const { WebpackManifestPlugin } = require('webpack-manifest-plugin');
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const fs = require('fs'); // to check if the file exists

module.exports = (env) => {
  // Get the root path (assuming your webpack config is in the root of your project!)
  const currentPath = path.join(__dirname);

  // Create the fallback path (the production .env)
  const basePath = currentPath + '/.env';

  // We're concatenating the environment name to our filename to specify the correct env file!
  const envPath = basePath + '.' + env.ENVIRONMENT;

  // Check if the file exists, otherwise fall back to the production .env
  const finalPath = fs.existsSync(envPath) ? envPath : basePath;

  // Set the path parameter in the dotenv config
  const fileEnv = dotenv.config({ path: finalPath }).parsed;

  // reduce it to a nice object, the same as before (but with the variables from the file)
  const envKeys = Object.keys(fileEnv).reduce((prev, next) => {
    prev[`process.env.${next}`] = JSON.stringify(fileEnv[next]);
    return prev;
  }, {});

  return {
    entry: './src/index.js',
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: 'bundle.js',
      publicPath: '/',
      sourceMapFilename: 'bundle.js.map'
    },

    optimization: {
      minimizer: [
        new TerserPlugin({
          terserOptions: {
            compress: {
              drop_console: false
            }
          }
        })
      ]
    },

    //devtool: 'hidden-source-map',
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: path.resolve(__dirname, 'node_modules/'),
          loader: 'babel-loader'
        },
        {
          test: /\.(woff(2)?|ttf|eot|png|svg|jpg|gif)(\?v=\d+\.\d+\.\d+)?$/,
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[name].[ext]',
                outputPath: 'fonts/'
              }
            }
          ]
        },
        // Include less-loader (exact settings may deviate depending on your building/bundling procedure)
        {
          test: /\.less$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
            },
            {
              loader: 'css-loader',
            },
            {
              loader: 'less-loader',
              options: { javascriptEnabled: true },
            },
          ],
        },
        {
          test: /\.scss$/,
          issuer: {
            exclude: /\.less$/
          },
          use: ['style-loader', 'css-loader', 'sass-loader']
        },
        {
          test: /\.css$/,
          use: ['style-loader', 'css-loader']
        },
        // Define a second rule for only being used from less files
        // This rule will only be used for converting our sass-variables to less-variables
        {
          test: /\.scss$/,
          issuer: /\.less$/,
          use: {
            loader: require.resolve('./sassVarsToLess') // Change path if necessary
          }
        },
        {
          test: /\.txt$/i,
          use: 'raw-loader'
        }
      ]
    },
    resolve: {
      extensions: ['.js', '.jsx']
    },
    //devtool: 'hidden-source-map',
    plugins: [
      new HtmlWebPackPlugin({
        template: path.resolve(__dirname, './public/index.html'),
        favicon: path.resolve(__dirname, './public/favicon.ico'),
        minify: {
          collapseWhitespace: 'auto'
        }
      }),
      new MiniCssExtractPlugin({
        ignoreOrder: true
      }),
      new FaviconsWebpackPlugin('./public/favicon.ico'),
      new Webpack.DefinePlugin(envKeys),
      new WebpackManifestPlugin({
        writeToFileEmit: true,
        seed: {}
      }),
      new CleanWebpackPlugin()
    ],

    devServer: {
      port: 3000,
      historyApiFallback: true
    }
  };
};
